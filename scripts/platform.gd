extends "monster.gd"
# parent class has _physics_process
# BUG - Player stays on platform but position may change slightly

const FLICKER_INTERVAL = 0.01
const PLAY_DEAD_TIME = 0.4
const PATH_SPEED = 40
const PATH_LENGTH = 130
var path_distance_travelled = 0
var path_direction = 1
var has_path = true

func _ready():
	_set_bounciness(0)
	_set_health    (2)
	_set_damage    (0)
	
	set_physics_process(true)
	

func _physics_process(delta):
	if has_path:
		# A rudimentary method to move back and forth
		path_distance_travelled += PATH_SPEED * delta
		if path_distance_travelled >= PATH_LENGTH:
			path_distance_travelled -= PATH_LENGTH
			path_direction = -path_direction
		move_and_collide(Vector2((PATH_SPEED * delta * path_direction), 0))
	

func handle_player_hit_enemy_top(player, enemy):
	pass
	

func handle_death():
	start_timer("death", PLAY_DEAD_TIME)
	play_dead()


func play_dead():
	flicker("death")
	sound_node.play()


func flicker(mode):
	if mode == "death":
		start_timer("flicker", FLICKER_INTERVAL)


func flicker_switch():
	idle_anim_node.visible = not idle_anim_node.visible


func handle_timeout(object_timer, name):
	if name == "death":
		die()
	elif name == "flicker":
		flicker_switch()
		start_timer("flicker", FLICKER_INTERVAL)
	object_timer.queue_free()


func die():
	self.queue_free()
