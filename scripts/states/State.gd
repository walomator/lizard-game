extends Node

# This state is generic. Another state may inherit this class, as it contains
# all required functions for the state to correctly interface with player.gd,
# with the exception of _init. It cannot be accessed by children from the
# parent class, so it must be created in all children. Include at least a
# simple _init that will set the controlling player in all states, as shown
# below:
# -----------------------------------------------------------------------------
#func _init(controlled_player):
#	player = controlled_player
# -----------------------------------------------------------------------------

# Any logic included within the functions of this state is template code. Leave
# the vars 'exiting' and 'player' as they are. Set the state name with
# set_state_name(new_string_name), where it matches the class name w/o ".gd".

# You must also add the following to player.gd for every state added:
# -----------------------------------------------------------------------------
#const NameOfState = preload("res://scripts/states/NameOfState.gd")
#
#...
#func set_state(new_state):
#	...
#	elif new_state == "NameOfState":
#		state = NameOfState.new(self)
#	...
# -----------------------------------------------------------------------------

var exiting = false # Prevents double state-setting
var player
var state_name = "GenericState"

func start():
	player.idle_sprite_node.visible = true # Must make something visible


func state_process(delta): # Called every _process(d) of the player
	pass


func handle_timeout(timer_name): # Called by timer after it times out
	pass


func set_state_name(new_state_name):
	state_name = new_state_name
	

func set_state(new_state):
	if exiting == true:
		return
	exiting = true

	player.idle_sprite_node.visible = false # Must hide everything previously set visible
	player.set_state(new_state)


func jump():
	player.default_jump()


func get_name():
	return state_name
